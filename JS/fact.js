/**
 * Fonction de vérification du nombre entré pour la factorielle
 */
function verifN() {
    var n;
    var verifPositif;

    n = document.getElementById("inputN").value;
    verifPositif = n >0;

    if (!verifPositif){
        document.getElementById("imgErrN").src ="../res/RedButton.svg";
        document.getElementById("erreurN").style.display="";
        document.getElementById("erreurN").innerText ="Il faut un entier";
    }else{
        document.getElementById("imgErrN").src ="../res/GreenButton.svg";
        document.getElementById("erreurN").style.display="none";
    }
    return verifPositif

}
/**
 * Fonction de calcul de factoriel pour le nombre de l'utilisateur
 */
function calcul() {
    var n;
    var fact = 1;

    n = document.getElementById("inputN").value;
    if (verifN()) {
        fact = factoriel(n);
        document.getElementById("resultat").style.display = '';
        document.getElementById("resultat").innerText = 'Factorielle de ' + n + " est égale à " + fact;
    }else{
        document.getElementById("resultat").innerText = "Veuiller d'abord regler les problèmes";
    }

}
/**
 * fonction calculant la factorielle d'un nombre donné
 */
function factoriel(n) {
        if (n === 0) {
            return 1;
        }
        else {
            return n * factoriel(n-1);
        }
}

/**
 * Fonction vidant le champ texte
 */
function clearAll() {
    document.getElementById("inputN").value ="";
}