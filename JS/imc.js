

/**
 * Fonction qui vérifie si la champ taille est conforme
 * Si le champ est vide ou contient autre chose que des nombres, le champ taill$
 *
 * Renvoie vrai si le champ est conforme, faux dans les autres cas
 */
function verifTaille(){
    var taille;
    var errTailleNull;
    var errTailleNombre;
    var errTaillePostif;
    var errTailleImpossible;
    let txtErrBase = "Le champ " ;
    var verifTaille;

    taille = document.getElementById("inputTaille").value;

    if (!(verifTaille =
            ((errTailleNull = (taille !== ""))
                && (errTailleNombre = (!isNaN(taille)))
                && (errTaillePostif = (taille > 0))
                && (errTailleImpossible = (taille < 3))))) {
        document.getElementById("erreurTaille").style.display ='';
        if (!errTailleNull) {
            txtErrTaille = " est obligatoire";
            //alert("Le champs taille est obligatoire");
        }
        else if (!errTailleNombre) {
            txtErrTaille = "  doit être un nombre";
            //alert("Le champs taille doit être un nombre");
        }
        else if (!errTaillePostif){
            txtErrTaille = "doit être positif";
            //alert("Le champs taille doit être positif");
        }
        else if (!errTailleImpossible){
            txtErrTaille = "ne peut pas dépassser 3 mètres";
            //alert("Le champs taille doit être positif");
        }
        document.getElementById("imgErrTaille").src ="../res/RedButton.svg";
        document.getElementById("erreurTaille").innerText = txtErrBase + "taille" + txtErrTaille;
    }
    else{
        document.getElementById("imgErrTaille").src ="../res/GreenButton.svg";
    }

    if (!verifTaille) {
        document.getElementById("inputTaille").style.color = 'red';
    }else{
        document.getElementById("inputTaille").style.color = '';
        document.getElementById("erreurTaille").style.display = 'none';
    }

    return verifTaille;
}
/**
 * Fonction qui vérifie si la champ masse est conforme
 * Si le champ est vide ou contient autre chose que des nombres, le champ masse$
 *
 * Renvoie vrai si le champ est conforme, faux dans les autres cas
 */

function verifMasse(){
    var masse;
    var verifMasse;
    var errTailleNull;
    var errTailleNombre;
    var errTaillePostif;
    let txtErrBase = "Le champ " ;

    masse = document.getElementById("inputMasse").value;

    if (!(verifMasse =
            (((errTailleNull = (masse !== ""))
                && (errTailleNombre = (!isNaN(masse)))
                && (errTaillePostif = (masse > 0))
            )))){
        document.getElementById("erreurMasse").style.display ='';
        if (!errTailleNull){
            txtErrMasse = " est obligatoire";
            //alert("Le champs masse est obligatoire");
        }
        else if (!errTailleNombre){
            txtErrMasse = "  doit être un nombre";
            //alert("Le champs masse doit être un nombre");
        }
        else if (!errTaillePostif){
            txtErrMasse = "doit être positif";
            //alert("Le champs masse doit être positif");
        }
        document.getElementById("imgErrMasse").src ="../res/RedButton.svg";
        document.getElementById("erreurMasse").innerText = txtErrBase + "masse" + txtErrMasse;


    }else{
        document.getElementById("imgErrMasse").src ="../res/GreenButton.svg";
    }
    if (!verifMasse) {
        document.getElementById("inputMasse").style.color = 'red';
    }else{
        document.getElementById("inputMasse").style.color = '';
        document.getElementById("erreurMasse").style.display = 'none';
    }

    return verifMasse;
}
/**
 * Fonction regroupant les deux fonctions de vérifications
 */

function verifEntrerChamps(){
    var bTaille = verifTaille();
    var bMasse = verifMasse();

    return (bMasse && bTaille);

}



/**
 * Fonction calculant l'IMC (Indice de masse corporelle)
 * Avec en paramètre le sexe , la taille et la masse entrés par l'utilisateur
 */

function calcul() {
    var taille;
    var masse;
    var imc;


    if (verifEntrerChamps()){
        taille = document.getElementById("inputTaille").value;
        masse = document.getElementById("inputMasse").value;
        imc = masse / Math.pow(taille,2);
        imc = parseFloat(imc).toFixed(2);

        afficheResultat(imc);
    } else{
        document.getElementById("resultat").innerText = "Veuiller d'abord regler les problèmes";
        document.getElementById("indication").style.display='none';
    }
}
/**
 * Fonction affichant le résultat en fonction de l'IMC calculeé
 */
function afficheResultat(IMC) {
    var imc =  IMC;
    var sexe;
    var txtImc;
    var SeuilMaigreur;
    var SeuilIdeal;
    var SeuilSurpoid;
    var SeuilObese;

    if (document.getElementById("rbHomme").checked === true) {
        sexe = "un homme ";
        SeuilMaigreur = 20.7;
        SeuilIdeal = 26.4;
        SeuilSurpoid = 27.8;
        SeuilObese = 31.1;
    } else {
        sexe = "une femme ";
        SeuilMaigreur = 19.1;
        SeuilIdeal = 25.8;
        SeuilSurpoid = 27.3;
        SeuilObese = 32.3;
    }


    document.getElementById("resultat").innerText = 'Votre IMC est égal à ' + imc + ".";
    if (imc < SeuilMaigreur) txtImc = 'maigre';
    else if (imc > SeuilMaigreur && imc < SeuilIdeal) txtImc = 'avec un poids idéal';
    else if (imc > SeuilIdeal && imc < SeuilSurpoid) txtImc = 'à la limite du surpoid';
    else if (imc > SeuilSurpoid && imc < SeuilObese) txtImc = 'en surpoids';
    else if (imc > SeuilObese) txtImc = 'obèse';
    document.getElementById("indication").style.display = '';
    document.getElementById("indication").innerText = 'Vous êtes ' + sexe + txtImc;
}

/**
 * Fonction attribuant un raccourci à CTRL-C
 * Permet de calculer l'IMC par ce raccourci
 */

function racourci(){
    var isCtrl = false;
    document.onkeyup=function(e){ if(e.which == 17) isCtrl=false; }
    document.onkeydown=function(e) {
        if (e.which == 17) isCtrl = true;
        if (e.which == 67 && isCtrl == true) {
            calcul()
        }
    }
}
/**
 * Fonction vidant les deux champs textes
 */

function clearAll(){
    document.getElementById("inputTaille").value = '';
    document.getElementById("inputMasse").value = '';
}
/**
 * Affiche une aide pour le calcul
 */

function afficherAideCalcul(){
    document.getElementById("aide").style.display = '';
}
/**
 * Enleve l'aide pour le calcul
 */

function removeAideCalcul(){
    document.getElementById("aide").style.display = 'none';
}

