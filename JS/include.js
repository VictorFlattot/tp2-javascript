/**
 * Fonction permettant l'initialisation du menu d'en-tete
 */
function initInclude() {
    includeHeader();
}
/**
 * Génération d'un menu d'en-tete
 */
function includeHeader() {
    document.getElementById("header").innerHTML= ""+
        "<ul id=\"main-nav\">"+
            "<li><a title=\"Action\">Actions</a>" +
                "<ul>" +
                    "<li><a onclick=\"calcul()\">calculer</a></li>" +
                    "<li><a onclick=\"clearAll()\">effacer</a></li>" +
                    "<li><a onclick=\"window.print()\">imprimer</a></li>" +
                "</ul>" +
            "</li>" +
            "<li><a title=\"Model\">Mode</a>" +
                "<ul class=\"dropdown-content\"> " +
                    "<li><a href=\"../HTML/imc.html\">calculer IMC</a></li>" +
                    "<li><a href=\"../HTML/fact.html\">calculer factorielle</a></li>" +
                    "<li><a href=\"https://calculis.net/fibonacci\">calculer fibonacci</a></li>" +
                    "<li><a href=\"../HTML/convert.html\">convertir</a></li>" +
                "</ul>" +
            "</li>" +
            "<li><a href=\"/truc.html\" title=\"Aide\">Aide</a>" +
                "<ul>" +
                    "<li><a href=\"../HTML/description.html\">description de site</a></li>" +
                    "<li><a href=\"../HTML/contact.html\">nous contacter</a></li>" +
                "</ul>" +
            "</li>" +
        "</ul>"
}

