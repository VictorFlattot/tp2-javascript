
/**
 * Fonction pour cahnger les labels affihcé en fonction de la liste déroulante
 */
function changeLabel() {
    var convertionDemande;
    var label1;
    var label2;

    convertionDemande = document.getElementById("convert").value;
    switch (convertionDemande) {
        case "cm -> inch":
            label1 = "cm : ";
            label2 = "inch : ";
            break;
        case "kg -> pound":
            label1 = "kg : ";
            label2 = "pound : ";
            break;
        case "km -> mile":
            label1 = "km : ";
            label2 = "mile : ";
            break;
        case "°C -> °F":
            label1 = "°C : ";
            label2 = "°F : ";
            break;
        default: break;
    }
    document.getElementById("lab1").innerText = label1;
    document.getElementById("lab2").innerText = label2;
    document.getElementById("resultat").style.display = "none";
}

/**
 * Fonction pour changer l"ordre de la convertion
 */
function echange(){
    var label1;
    var label2;
    var tmpTxtLabel;

    label1 = document.getElementById("lab1");
    label2 = document.getElementById("lab2");

    tmpTxtLabel = label1.textContent;
    label1.innerText = label2.textContent;
    label2.innerText = tmpTxtLabel;

}
/**
 * Fonction de calcul de la convertion en fonction de la convertion demandé
 */
function calcul() {
    var convertionDemande;
    var input;
    var cooefConvertion = 0;

    input = document.getElementById("input1").value;
    convertionDemande = document.getElementById("convert").value;

    switch (convertionDemande) {
        case "cm -> inch":
            cooefConvertion = 2.54;
            break;
        case "kg -> pound":
            cooefConvertion = 2.20462262;
            break;
        case "km -> mile":
            cooefConvertion = 1.609344;
            break;
    }
    if (cooefConvertion !== 0) {
        convert(input,cooefConvertion);
    }else{
        convertTemperature(input);
    }


}

/**
 * Fonction de convertion pour les unités de mesure de longueur et de masse
 */
function convert(input,cooef) {
    var txtLabel1;
    var resultat;

    txtLabel1 = document.getElementById("lab1").innerText;
    if ((txtLabel1==="cm : ") ||
        (txtLabel1==="pound : ") ||
        (txtLabel1==="km : ") ){
        resultat = input / cooef;
    }else{
    resultat = input * cooef;
}

document.getElementById("resultat").style.display = "";
document.getElementById("resultat").innerText = resultat;
}
/**
 * Fonction de convertion pour les unités de temperature
 */
function convertTemperature(input){
    var txtLabel1;
    var resultat;

    txtLabel1 = document.getElementById("lab1").innerText;
    if (txtLabel1==="°F : "){
        resultat = input *1.8 +32;
    }else{
        resultat = (input - 32)/1.8;
    }

    document.getElementById("resultat").style.display = "";
    document.getElementById("resultat").innerText = resultat;
}



